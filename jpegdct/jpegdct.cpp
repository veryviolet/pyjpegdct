#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <exception>
#include <jpeglib.h>


bool jpeg_load(const std::string & fname, struct jpeg_decompress_struct * p_file_info, FILE * pF)
{
    struct jpeg_error_mgr jerr;

    pF = NULL;

    try
    {
        p_file_info->err = jpeg_std_error(&jerr);
        jpeg_create_decompress(p_file_info);
        if ((pF = fopen(fname.c_str(), "rb")) == NULL)
            throw("failed to open file");

        jpeg_stdio_src(p_file_info, pF);
        (void) jpeg_read_header(p_file_info, TRUE);

    }
    catch(std::exception e)
    {
        std::cerr << e.what();
        return false;
    }

    return true;
}

void destroy(struct jpeg_decompress_struct * p_file_info, FILE * pF)
{
    jpeg_finish_decompress(p_file_info);
    jpeg_destroy_decompress(p_file_info);

    if (pF != NULL) fclose(pF);
}


bool extract_dct(struct jpeg_decompress_struct * p_file_info, py::list & dct)
{
    try
    {
        jvirt_barray_ptr * coef_arrays = jpeg_read_coefficients(p_file_info);

        for (auto c = 0; c < p_file_info->num_components; c++)
        {
            auto height_in_blocks = p_file_info->comp_info[c].height_in_blocks;
            auto width_in_blocks = p_file_info->comp_info[c].width_in_blocks;
            auto height = height_in_blocks * DCTSIZE;
            auto width = width_in_blocks * DCTSIZE;

            double * arr = new double[height*width];

            printf("\n * allocated dct: %p", (void *) arr);


            for(JDIMENSION blk_y = 0; blk_y < height_in_blocks; blk_y ++)
            {
                JBLOCKARRAY ybuffer = (p_file_info->mem->access_virt_barray)((j_common_ptr) p_file_info,
                                                                   coef_arrays[c], blk_y, 1, FALSE);
                for(JDIMENSION blk_x = 0; blk_x < width_in_blocks; blk_x ++)
                {
                    JCOEFPTR bufptr = ybuffer[0][blk_x];
                    for (auto i = 0; i < DCTSIZE; i++)
                        for (auto j = 0; j < DCTSIZE; j++)
                            arr[(blk_y*DCTSIZE+i)*width + (blk_x*DCTSIZE+j)] = (double) bufptr[i*DCTSIZE+j];
                }
            }

            py::capsule free_when_done(arr, [](void *f) {
                double *foo = reinterpret_cast<double *>(f);
                delete[] foo;
                printf("\n * deleted dct: %p", (void *) foo);
            });

            dct.append(py::array_t<double>(
                {height, width},
                {width*sizeof(double), sizeof(double)},
                arr,
                free_when_done));
        }
    }
    catch(std::exception e)
    {
        std::cerr << e.what();
        return false;
    }

   return true;
}

bool extract_qt(struct jpeg_decompress_struct * p_file_info, py::list & qtables)
{

    try
    {
        for (auto c = 0; c < NUM_QUANT_TBLS; c++)
        {
	        JQUANT_TBL *quant_ptr;

            quant_ptr = p_file_info->quant_tbl_ptrs[c];
            if(quant_ptr != NULL)
            {
	            double * arr = new double[DCTSIZE*DCTSIZE];
                printf("\n * allocated qt: %p", (void *) arr);


	            for (int i = 0; i < DCTSIZE; i++)
	               for (int j = 0; j < DCTSIZE; j++)
	                    arr[j*DCTSIZE+i] = (double) quant_ptr->quantval[i*DCTSIZE+j];

	            py::capsule free_when_done(arr, [](void *f) {
	                double *foo = reinterpret_cast<double *>(f);
	                delete[] foo;
                    printf("\n * deleted qt: %p", (void *) foo);
	            });

	            qtables.append(py::array_t<double>(
	                {DCTSIZE, DCTSIZE},
	                {DCTSIZE*sizeof(double), sizeof(double)},
	                arr,
	                free_when_done));

            }
        }
    }
    catch(std::exception e)
    {
        std::cerr << e.what();
        return false;
    }

   return true;


}

py::tuple get_data(py::str filename)
{
        py::list qtables = py::list();
        py::list dct = py::list();

		FILE *pF = NULL;        
        struct jpeg_decompress_struct file_info;

        if(jpeg_load(filename, &file_info, pF))
            extract_dct(&file_info, dct);
            extract_qt(&file_info, qtables);

		destroy(&file_info, pF);

        return py::make_tuple(qtables, dct);

}


PYBIND11_MODULE(pyjpegdct, m) {
    m.def("get_data", &get_data);
}